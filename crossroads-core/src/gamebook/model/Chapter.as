package gamebook.model
{
	public class Chapter
	{
		private var _reference : String;
		
		private var _name : String;
		
		private var _locationX : Number;

		private var _locationY : Number;
		
		private var _reachableMarkerImage : String;		
		
		private var _unreachableMarkerImage : String;
		
		private var _initiallyDiscovered : Boolean;

		private var _adjacentChapterReferences : Array;
		
		private var _scenes : Array;
		
		
		public function Chapter(reference : String, name : String, locationX : Number, locationY : Number,
								reachableMarkerImage : String, unreachableMarkerImage : String, initiallyDiscovered : Boolean,
								adjacentChapterReferences : Array, scenes : Array)
		{
			_reference = reference;
			_name = name;
			_locationX = locationX;
			_locationY = locationY;
			_reachableMarkerImage = reachableMarkerImage;
			_unreachableMarkerImage = unreachableMarkerImage;
			_initiallyDiscovered = initiallyDiscovered;
			_adjacentChapterReferences = adjacentChapterReferences;
			_scenes = scenes;
		}
		
		
		public function get scenes():Array
		{
			return _scenes;
		}

		public function get adjacentChapterReferences():Array
		{
			return _adjacentChapterReferences;
		}

		public function get initiallyDiscovered():Boolean
		{
			return _initiallyDiscovered;
		}

		public function get unreachableMarkerImage():String
		{
			return _unreachableMarkerImage;
		}

		public function get reachableMarkerImage():String
		{
			return _reachableMarkerImage;
		}

		public function get locationY():Number
		{
			return _locationY;
		}

		public function get locationX():Number
		{
			return _locationX;
		}

		public function get name():String
		{
			return _name;
		}

		public function get reference():String
		{
			return _reference;
		}

		public function toString() : String
		{
			return "CHAPTER " + reference + ": " + name + " @ " + locationX + " at " + locationY + "\n"
				+ "  R [" + reachableMarkerImage + "] U [" + unreachableMarkerImage + "]. "
				+ (initiallyDiscovered ? "Discovered" : "Hidden") + " adjacent to " + adjacentChapterReferences.join("|") + ".";
		}
	}
}
