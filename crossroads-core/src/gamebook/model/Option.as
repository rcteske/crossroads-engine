package gamebook.model
{
	public class Option
	{
		private var _reference : String;
		
		private var _text : String;
		
		private var _conditions : String;			
		
		private var _effects : String;
		
		private var _nextScene : String;
		
		
		public function Option(reference : String, text : String, conditions : String, effects : String, nextScene : String)
		{
			_reference = reference;
			_text = text;
			_conditions = conditions;
			_effects = effects;
			_nextScene = nextScene;
		}
		
		
		public function get nextScene():String
		{
			return _nextScene;
		}

		public function get effects():String
		{
			return _effects;
		}

		public function get conditions():String
		{
			return _conditions;
		}

		public function get text():String
		{
			return _text;
		}

		public function get reference():String
		{
			return _reference;
		}

		public function toString() : String
		{
			return "OPTION " + reference + ": " + text + " (goes to " + nextScene + ")";
		}
	}
}
