package gamebook.model
{
	public class Scene
	{
		private var _reference : String;
		
		private var _text : String;
		
		private var _title : String;		
		
		private var _image : String;
		
		private var _initial : Boolean;
		
		private var _initialConditions : String;
		
		private var _options : Array = new Array();
		
		
		public function Scene(reference : String, title : String, text : String, image : String, initial : Boolean, initialConditions : String)
		{
			_reference = reference;
			_title = title;
			_text = text;
			_image = image;
			_initial = initial;
			_initialConditions = initialConditions;			
		}
		
		
		public function get options():Array
		{
			return _options;
		}

		public function get initialConditions():String
		{
			return _initialConditions;
		}

		public function get initial():Boolean
		{
			return _initial;
		}

		public function get image():String
		{
			return _image;
		}

		public function get title():String
		{
			return _title;
		}

		public function get text():String
		{
			return _text;
		}

		public function get reference():String
		{
			return _reference;
		}

		public function toString() : String
		{
			var imageString : String = (image != "" ? " I [" + image+ "]." : "");
			var initialString : String = (initial ? "INITIAL " : "");
			
			return initialString + "SCENE " + title + " (" + reference + "): " + text + "." + imageString;
		}
	}
}
