package gamebook.model
{
	public class StaticContent
	{
		private var _chapters : Array = new Array();
		
		private var _chaptersHash : Array = new Array();
		
		private var _scenesHash : Array = new Array();
		
		private var _chapterCount : int = 0;
		
		private var _sceneCount : int = 0;
		
		
		public function StaticContent(chapters : Array, chaptersHash : Array, chapterCount : int, scenesHash : Array, sceneCount : int)
		{
			_chapters = chapters;
			_chaptersHash = chaptersHash;
			_chapterCount = chapterCount;
			_scenesHash = scenesHash;
			_sceneCount = sceneCount;
		}		

		public function get sceneCount():int
		{
			return _sceneCount;
		}

		public function get chapterCount():int
		{
			return _chapterCount;
		}

		public function get scenesHash() : Array
		{
			return _scenesHash;
		}

		public function get chaptersHash() : Array
		{
			return _chaptersHash;
		}

		public function get chapters() : Array
		{
			return _chapters;
		}
	}
}
