package gamebook.model.condition
{
	public class Condition
	{
		public function evaluate(variables : Array) : Boolean
		{
			// Override this to create new conditions.
			return false;
		}
	}
}
