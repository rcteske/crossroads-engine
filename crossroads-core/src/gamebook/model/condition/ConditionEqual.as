package gamebook.model.condition
{
	import gamebook.util.StringHelper;

	public class ConditionEqual extends Condition
	{
		import gamebook.util.Log;
		
		import mx.utils.StringUtil;
	
		
		private var variableName : String;
		
		private var value : Number;
		
		
		public static function check(condition : String) : Boolean
		{
			var index : int = condition.indexOf("=");
		
			return (index > 0) && (condition.indexOf("<=") == -1) && (condition.indexOf(">=") == -1) && (index < condition.length);
		}
		
		
		public function ConditionEqual(condition : String)
		{
			var conditionPieces : Array = condition.split("=");
			
			variableName = StringUtil.trim(conditionPieces[0]);
			var valuePiece : String = StringUtil.trim(conditionPieces[1]);
			
			if (isNaN(Number(valuePiece)))
			{
				value = 0;
			}
			else
			{
				value = Number(valuePiece);
			}
		}
		
		
		public override function evaluate(variables : Array) : Boolean
		{
			var currentValue : Number;
			
			if ((StringHelper.isEmpty(variables[variableName])) || (isNaN(Number(variables[variableName]))))
			{
				currentValue = 0;
			}
			else
			{
				currentValue = Number(variables[variableName]);
			}
			
			var result : Boolean = (currentValue == value);
			
			Log.debug("Condition " + variableName + " (" + currentValue + ") = " + value + " is " + (result ? "true" : "false"));
			
			return result;
		}
	}
}
