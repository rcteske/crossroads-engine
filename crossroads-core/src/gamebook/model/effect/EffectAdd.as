package gamebook.model.effect
{
	import gamebook.util.StringHelper;

	public class EffectAdd extends Effect
	{
		import gamebook.util.Log;
		
		import mx.utils.StringUtil;
		
		
		private var variableName : String;
		
		private var value : Number;
		
		
		public static function check(effect : String) : Boolean
		{
			var index : int = effect.indexOf("+");
			
			return (index > 0) && (index < effect.length);
		}
		
		
		public function EffectAdd(effect : String)
		{
			// Assignment.
			var effectPieces : Array = effect.split("+");
			
			variableName = StringUtil.trim(effectPieces[0]);
			var valuePiece : String = StringUtil.trim(effectPieces[1]);
			
			if (isNaN(Number(valuePiece)))
			{
				value = 0;
			}
			else
			{
				value = Number(valuePiece);
			}
		}
		
		
		public override function apply(variables : Array) : void
		{
			var currentValue : Number;
			
			if ((StringHelper.isEmpty(variables[variableName])) || (isNaN(Number(variables[variableName]))))
			{
				currentValue = 0;
			}
			else
			{
				currentValue = Number(variables[variableName]);
			}
			
			var result : Number = currentValue + value;
			
			Log.debug("Add " + variableName + " (" + currentValue + ") + " + value + " = " + result);
			
			variables[variableName] = result;
		}
	}
}
