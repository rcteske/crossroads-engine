package gamebook.model.effect
{
	import gamebook.util.StringHelper;

	public class EffectAssign extends Effect
	{
		import gamebook.util.Log;
		
		import mx.utils.StringUtil;
		
		
		private var variableName : String;
		
		private var value : Number;
		
		
		public static function check(effect : String) : Boolean
		{
			var index : int = effect.indexOf("=");
			
			return (index > 0) && (index < effect.length);
		}
		
		
		public function EffectAssign(effect : String)
		{
			// Assignment.
			var effectPieces : Array = effect.split("=");
			
			variableName = StringUtil.trim(effectPieces[0]);
			var valuePiece : String = StringUtil.trim(effectPieces[1]);
			
			if (isNaN(Number(valuePiece)))
			{
				value = 0;
			}
			else
			{
				value = Number(valuePiece);
			}
		}
		
		
		public override function apply(variables : Array) : void
		{
			Log.debug("Assign " + variableName + " = " + value);
			
			variables[variableName] = value;
		}
	}
}
