package gamebook.util
{
	import gamebook.model.Chapter;
	import gamebook.model.Option;
	import gamebook.model.Scene;

	public class ContentParser
	{
		import gamebook.model.Chapter;
		import gamebook.model.StaticContent;		
		
		import mx.utils.StringUtil;
		
		
		protected static var ChapterReferenceColumn : int = 0;
		
		protected static var ChapterNameColumn : int = 1;
		
		protected static var ChapterLocationXColumn : int = 2;
		
		protected static var ChapterLocationYColumn : int = 3;			
		
		protected static var ChapterReachableMarkerColumn : int = 4;
		
		protected static var ChapterUnreachableMarkerColumn : int = 5;
		
		protected static var ChapterDiscoveredColumn : int = 6;
		
		protected static var AdjacentChapterColumn : int = 7;
		
		
		protected static var SceneReferenceColumn : int = 8;
		
		protected static var SceneTitleColumn : int = 9;			
		
		protected static var SceneTextColumn : int = 10;			
		
		protected static var SceneImageColumn : int = 11;
		
		protected static var InitialSceneColumn : int = 12;
		
		protected static var InitialSceneConditionColumn : int = 13;
		
		
		protected static var OptionReferenceColumn : int = 14;
		
		protected static var OptionTextColumn : int = 15;
		
		protected static var OptionConditionColumn : int = 16;
		
		protected static var OptionEffectColumn : int = 17;
		
		protected static var OptionNextSceneColumn : int = 18;
		
		
		public static function parse(content : String) : StaticContent
		{
			var chaptersHash : Array = new Array();
			var chapters : Array = new Array();
			var scenesHash : Array = new Array();
			
			var chapterCount : int = 0;
			var sceneCount : int = 0;
			
			var currentChapter : Chapter;
			var currentScene : Scene;
			
			var values : Array;			
			var lines : Array = content.split("\r\n");
			
			for each (var line : String in lines)
			{
				if ((StringHelper.isEmpty(line)) || (line == lines[0]))
				{
					// Ignore first line (titles) and empty lines.
					continue;
				}
				
				values = line.split(";");
				
				// Chapter.
				var newChapter : Chapter = ContentParser.parseChapter(values, currentChapter, chaptersHash);
				
				if (newChapter != null)
				{
					currentChapter = newChapter;
					chapters.push(currentChapter);
					chapterCount++;
				}
				
				// Scene.
				var newScene : gamebook.model.Scene = ContentParser.parseScene(values, currentScene, scenesHash); 
				
				if (newScene != null) {
					currentScene = newScene;
					currentChapter.scenes.push(currentScene);					
					sceneCount++;
				}
				
				// Option.
				var newOption : Option = ContentParser.parseOption(values, currentScene);					
				
				if (newOption != null)
				{
					currentScene.options.push(newOption);	
				}
			}
			
			return new StaticContent(chapters, chaptersHash, chapterCount, scenesHash, sceneCount);
		}
		
		
		public static function parseChapter(values : Array, currentChapter : Chapter, chaptersHash : Array) : Chapter
		{
			/*
			0 Chapter reference
			1 Chapter name
			2 Location X
			3 Location Y
			4 Reachable marker image
			5 Unreachable marker image
			6 Discovered?
			7 Adjacent chapters
			*/
			
			var chapterReference : String = values[ChapterReferenceColumn];
			chapterReference = StringUtil.trim(chapterReference);
			
			if ((!StringHelper.isEmpty(chapterReference)) && ((currentChapter == null) || (chapterReference != currentChapter.reference))) {
				currentChapter = chaptersHash[chapterReference];
				
				if (currentChapter == null) {
					var locationX : Number = Number(values[ChapterLocationXColumn]);
					
					if (isNaN(locationX)) {
						locationX = 0.5;
					}

					var locationY : Number = Number(values[ChapterLocationYColumn]);
					
					if (isNaN(locationY)) {
						locationY = 0.5;
					}
					
					var discovered : Boolean = StringHelper.isTrue(values[ChapterDiscoveredColumn]);
					var adjacentChapters : Array = (values[AdjacentChapterColumn]).split("|");
					
					var chapterName : String = values[ChapterNameColumn];
					var reachableMarker : String = values[ChapterReachableMarkerColumn];
					var unreachableMarker : String = values[ChapterUnreachableMarkerColumn];
					
					var scenes : Array = new Array();
					
					currentChapter = new Chapter(chapterReference, chapterName, locationX, locationY, reachableMarker,
						unreachableMarker, discovered, adjacentChapters, scenes);
					
					chaptersHash[currentChapter.reference] = currentChapter;
					
					return currentChapter;
				}
			}
			
			return null;
		}
		
		
		public static function parseScene(values : Array, currentScene : gamebook.model.Scene, scenesHash : Array) : gamebook.model.Scene {
			/*
			8 Scene reference
			9 Scene title
			10 Scene text
			11 Scene image
			12 Initial scene?
			13 Initial scene preconditions
			*/
			
			var sceneReference : String = StringUtil.trim(values[SceneReferenceColumn]);
			
			if ((!StringHelper.isEmpty(sceneReference)) && ((currentScene == null) || (sceneReference != currentScene.reference))) {
				currentScene = scenesHash[sceneReference];
				
				if (currentScene == null) {
					var isInitialScene : Boolean = (values[InitialSceneColumn] == "1") || (values[InitialSceneColumn] == "TRUE") || (values[InitialSceneColumn] == "VERDADEIRO");
					var sceneText : String = StringHelper.replaceAll(values[SceneTextColumn], "\\n", "\n");
					
					currentScene = new gamebook.model.Scene(sceneReference, StringUtil.trim(values[SceneTitleColumn]), sceneText, values[SceneImageColumn], isInitialScene, values[InitialSceneConditionColumn]);
					scenesHash[currentScene.reference] = currentScene;

					return currentScene;
				}
			}
			
			return currentScene;
		}
		
		
		public static function parseOption(values : Array, currentScene : gamebook.model.Scene) : Option {
			/*
			14 Option reference
			15 Option text
			16 Option preconditions
			17 Option effects
			18 Option next scene
			*/
			
			var optionText : String = StringUtil.trim(values[OptionTextColumn]);
			
			var currentOption : Option = null;
			
			if (!StringHelper.isEmpty(optionText)) {
				var optionReference : String = StringUtil.trim(values[OptionReferenceColumn]);
				
				if ((optionReference == null) || (optionReference == "")) {
					optionReference = currentScene.reference + "." + int(Math.random() * 1000);
				}
				
				currentOption = new Option(optionReference, optionText, values[OptionConditionColumn], values[OptionEffectColumn], values[OptionNextSceneColumn]);
			}
			
			return currentOption;
		}
		
		
		public static function logContents(contents : StaticContent) : void {
			for each (var chapter : Chapter in contents.chapters) {
				Log.debug("=======================================================");
				
				Log.debug("* " + chapter.toString() + "\n");
				
				for each (var scene : gamebook.model.Scene in chapter.scenes) {
					Log.debug("* " + scene.toString() + "\n");
					
					for each (var option : Option in scene.options) {
						Log.debug("* " + option.toString());
					}
					
					Log.debug(" ");
				}
				
				Log.debug("=======================================================\n");
			}
		}
	}
}
