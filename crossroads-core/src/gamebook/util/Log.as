package gamebook.util
{
	import spark.components.TextArea;

	public class Log
	{
		public static var outputArea : TextArea = null;
		
		public static var componentOutput : Boolean = false;
		
		
		public static function debug(message : String) : void
		{			
			if (!componentOutput) {
				trace(message);
				
			} else {
				outputArea.appendText(message + "\n");	
			}
		}
	}
}
