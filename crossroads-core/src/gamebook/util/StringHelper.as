package gamebook.util
{
	public class StringHelper
	{
		public static function isEmpty(string : String) : Boolean
		{
			return (string == null) || (string.length == 0);
		}
		
		
		public static function isTrue(string : String) : Boolean
		{
			if (isEmpty(string))
			{
				return false;
			}
			
			return (string == "1") || (string == "TRUE") || (string == "VERDADEIRO");	
		}
		
		
		public static function replaceAll(source:String, find:String, replacement:String) : String
		{
			return source.split(find).join(replacement);
		}
		
		
		public static function parseBoolean(value : String) : Boolean
		{
			switch(value.toLowerCase())
			{
				case "1":
				case "true":
				case "yes":
					return true;
					
				case "0":
				case "false":
				case "no":
					return false;
				
				default:
					return Boolean(value);
			}
		}
	}
}
