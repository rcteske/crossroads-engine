package gamebook.editor
{
	public final class EditorDefaults
	{
		public static var NodeBoxWidth : Number = 140;
		
		public static var NodeBoxHeight : Number = 160;
		
		public static var SceneColor : uint = 0xFFFF99;
		
		public static var SelectedSceneColor : uint = 0xFFFF00;
		
		public static var LineToSceneColor : uint = 0;

		public static var OptionColor : uint = 0xC2EBFF;
		
		public static var SelectedOptionColor : uint = 0x85D6FF;
		
		public static var LineToOptionColor : uint = 0x85D6FF;
		
		public static var NodeConnectorRadius : int = 6;
		
		public static var BezierSteps : uint = 20;
		
		public static var ConnectionAsBezier : Boolean = true;
		
		public static var BezierControlDistance : int = 100;
	}
}
