package gamebook.editor
{
	public final class InterfaceStates
	{
		public static var Idle : int = -1;
		
		public static var AddingScene : int = -2;
		
		public static var AddingOption : int = -3;
		
		public static var Dragging : int = -4;
	}
}
