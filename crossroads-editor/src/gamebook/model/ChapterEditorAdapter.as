package gamebook.model
{
	import flash.utils.Dictionary;

	public class ChapterEditorAdapter
	{
		private var _chapter : Chapter;
		
		private var _scenes : Dictionary = new Dictionary();
		
		private var _options : Dictionary = new Dictionary();
		
		
		public function ChapterEditorAdapter(chapter : Chapter)
		{
			_chapter = chapter;
		}

		public function get scenes():Dictionary
		{
			return _scenes;
		}

		public function get options():Dictionary
		{
			return _options;
		}

		public function get chapter():Chapter
		{
			return _chapter;
		}

		public function set chapter(value:Chapter):void
		{
			_chapter = value;
		}


	}
}