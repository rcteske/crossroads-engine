package gamebook.model
{
	import mx.core.UIComponent;

	public class OptionEditorAdapter
	{
		private var _option : Option;
		
		private var _nodeLocationX : int;
		
		private var _nodeLocationY : int;
		
		private var _sourceScenesReferences : Array = new Array();
		
		private var _nodeComponent : UIComponent;
		
		public function OptionEditorAdapter(option : Option, nodeLocationX : int, nodeLocationY : int)
		{
			_option = option;
			_nodeLocationX = nodeLocationX;
			_nodeLocationY = nodeLocationY;
		}

		public function get nodeComponent():UIComponent
		{
			return _nodeComponent;
		}

		public function set nodeComponent(value:UIComponent):void
		{
			_nodeComponent = value;
		}

		public function get sourceScenesReferences():Array
		{
			return _sourceScenesReferences;
		}

		public function set option(value:Option):void
		{
			_option = value;
		}

		public function get option():Option
		{
			return _option;
		}
		
		public function set nodeLocationY(value:int):void
		{
			_nodeLocationY = value;
		}

		public function get nodeLocationX():int
		{
			return _nodeLocationX;
		}

		public function set nodeLocationX(value:int):void
		{
			_nodeLocationX = value;
		}

		public function get nodeLocationY():int
		{
			return _nodeLocationY;
		}

		public function set sourceScenesReferences(value:Array):void
		{
			_sourceScenesReferences = value;
		}
	}
}
