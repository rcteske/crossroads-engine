package gamebook.model
{
	import mx.core.UIComponent;

	public class SceneEditorAdapter
	{
		private var _scene : Scene;
		
		private var _nodeLocationX : int;
		
		private var _nodeLocationY : int;
		
		private var _nodeComponent : UIComponent;
		
		public function SceneEditorAdapter(scene : Scene, nodeLocationX : int, nodeLocationY : int)
		{
			_scene = scene;
			_nodeLocationX = nodeLocationX;
			_nodeLocationY = nodeLocationY;
		}

		public function get nodeComponent():UIComponent
		{
			return _nodeComponent;
		}

		public function set nodeComponent(value:UIComponent):void
		{
			_nodeComponent = value;
		}

		public function set scene(value:Scene):void
		{
			_scene = value;
		}

		public function get scene():Scene
		{
			return _scene;
		}
		
		public function set nodeLocationY(value:int):void
		{
			_nodeLocationY = value;
		}

		public function get nodeLocationX():int
		{
			return _nodeLocationX;
		}

		public function set nodeLocationX(value:int):void
		{
			_nodeLocationX = value;
		}

		public function get nodeLocationY():int
		{
			return _nodeLocationY;
		}
	}
}
