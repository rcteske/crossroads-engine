package gamebook.util
{
	import gamebook.model.Chapter;
	import gamebook.model.ChapterEditorAdapter;
	import gamebook.model.Option;
	import gamebook.model.OptionEditorAdapter;
	import gamebook.model.Scene;
	import gamebook.model.SceneEditorAdapter;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;

	public class EditorContentParser
	{
		private static var EditorSceneNodeLocationXColumn : int = 19;
		
		private static var EditorSceneNodeLocationYColumn : int = 20;
		
		private static var EditorOptionNodeLocationXColumn : int = 21;
		
		private static var EditorOptionNodeLocationYColumn : int = 22;
		
		
		public static function parseForEditor(content : String, width : int, height : int) : ArrayCollection
		{
			var chaptersHash : Array = new Array();
			var chapters : ArrayCollection = new ArrayCollection();
			var scenesHash : Array = new Array();
			
			var chapterCount : int = 0;
			var sceneCount : int = 0;
			
			var currentChapter : ChapterEditorAdapter;
			var currentScene : Scene;
			
			var values : Array;			
			var lines : Array = content.split("\r\n");
			
			var columnCount : int = 0;
			var x : int;
			var y : int;
			
			for each (var line : String in lines)
			{
				if ((StringHelper.isEmpty(line)) || (line == lines[0]))
				{
					// Ignore first line (titles) and empty lines.
					continue;
				}
				
				values = line.split(";");
				
				if (columnCount == 0)
				{
					columnCount = values.length;
				}
				
				// Chapter.
				var newChapter : Chapter = ContentParser.parseChapter(values, (currentChapter != null ? currentChapter.chapter : null), chaptersHash);
				
				if (newChapter != null)
				{
					currentChapter = new ChapterEditorAdapter(newChapter);
					chapters.addItem(currentChapter);
				}
				
				// Scene.
				var newScene : gamebook.model.Scene = ContentParser.parseScene(values, currentScene, scenesHash); 
				
				if (newScene != null) {
					currentScene = newScene;
					
					if (columnCount <= EditorSceneNodeLocationXColumn)
					{
						x = Math.random() * width;
						y = Math.random() * height;
					}
					else
					{
						x = Number(values[EditorSceneNodeLocationXColumn]);
						y = Number(values[EditorSceneNodeLocationYColumn]);
						
						if (isNaN(x)) {
							x = Math.random() * width;
						}
						
						if (isNaN(y)) {
							y = Math.random() * height;
						}
					}
					
					currentChapter.scenes[currentScene.reference] = new SceneEditorAdapter(currentScene, x, y);					
				}
				
				// Option.
				var newOption : Option = ContentParser.parseOption(values, currentScene);					
				
				if (newOption != null)
				{
					if (columnCount <= EditorSceneNodeLocationXColumn)
					{
						x = Math.random() * width;
						y = Math.random() * height;
					}
					else
					{
						x = Number(values[EditorOptionNodeLocationXColumn]);
						y = Number(values[EditorOptionNodeLocationYColumn]);
						
						if (isNaN(x)) {
							x = Math.random() * width;
						}
						
						if (isNaN(y)) {
							y = Math.random() * height;
						}
					}
					
					var currentOption : OptionEditorAdapter = new OptionEditorAdapter(newOption, x, y);  
					currentChapter.options[newOption.reference] = currentOption;
					currentOption.sourceScenesReferences.push(currentScene.reference);
				}
			}
			
			return chapters;
		}
	}
}
