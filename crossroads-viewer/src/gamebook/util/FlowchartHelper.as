package gamebook.util
{
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;

	public class FlowchartHelper
	{
		import gamebook.model.Chapter;
		import gamebook.model.Option;
		import gamebook.model.Scene;
		import gamebook.model.StaticContent;
		
		import flash.filesystem.File;
		
		
		private var staticContent : StaticContent;
		
		private var currentScene : Scene;
		
		
		public function FlowchartHelper(_staticContent : StaticContent, _currentScene : Scene)
		{
			staticContent = _staticContent;
			currentScene = _currentScene;
		}

		
		public function saveContentFlowchart() : void
		{
			var fileTarget : File = new File();
			
			try 
			{
				fileTarget.browseForSave("Save flowchart");
				fileTarget.addEventListener(Event.SELECT, chartFileSelected);
			}
			catch (error:Error)
			{
				Log.debug("Failed to save flowchart: " + error.message + ".\n");
			}
		}

		
		private function chartFileSelected(event:Event) : void {
			var graphSource : String = generateGraphSource();
			var stream : FileStream = new FileStream();
			
			stream.open(event.target as File, FileMode.WRITE);
			stream.writeUTFBytes(graphSource);
			stream.close();
			
			Log.debug("Flowchart file saved.");
		}
		
		
		private function generateGraphSource() : String
		{
			var graphSource : String = graphSource = "digraph Gamebook {\r\n";
			graphSource += "overlap = false;\r\n";
			
			// Default nodes.
			graphSource += "node [shape = ellipse, fixedsize = false, fillcolor = cyan, style = \"rounded, filled\"]; GameStart;\r\n";
			graphSource += "node [shape = ellipse, fixedsize = false, fillcolor = cyan, style = \"rounded, filled\"]; GameEnd;\r\n";
			
			// Scene nodes.
			graphSource += "node [shape = rect, fixedsize = false, fillcolor = white, style = \"rounded, filled\"]; \"Map\"; ";
			
			var transitionNodes : String = "node [shape = diamond, fixedsize = false, fillcolor = yellow, style = \"rounded, filled\"]; ";
			
			for each (var chapter : Chapter in staticContent.chapters) {
				for each (var scene : gamebook.model.Scene in chapter.scenes) {
					graphSource += "\"" + scene.reference + "\"; ";
					
					for each (var option : Option in scene.options) {
						transitionNodes += "\"" + option.reference + "\"; ";
					}
				}
			}
			
			graphSource += "\r\n" + transitionNodes + "\r\n";
			
			// Edges.
			graphSource += "GameStart -> Map; Map -> GameEnd;\r\n";
			graphSource += "GameStart -> \"" + currentScene.reference + "\";\r\n";
			
			var nextScene : String;
			
			for each (chapter in staticContent.chapters) {
				graphSource += "subgraph cluster_" + chapter.reference + " {\r\n";
				graphSource += "style = rounded;\r\n";						
				
				for each (scene in chapter.scenes) {
					for each (option in scene.options) {
						nextScene = option.nextScene;
						
						if ((nextScene == null) || (nextScene == "")) {
							nextScene = "Map";
						}
						
						graphSource += "\"" + scene.reference + "\" -> \"" + option.reference + "\";\r\n";
						graphSource += "\"" + option.reference + "\" -> \"" + nextScene + "\";\r\n";
					}
				}
				
				graphSource += "label = \"Chapter " + chapter.reference + "\"\r\n}\r\n";
			}
			
			graphSource += "}\r\n";
			
			return graphSource;
		}
	}
}
